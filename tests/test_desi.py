from thermo.chemical import Chemical

from pandadheating.design import *

water = Chemical("water", T=60 + 273.15, P=1.0e5)

def test_design_pipe():
    m_dot, diam, diam_precise = design_pipe(water, 1E3)
    assert round(m_dot, 3) == 0.016
    assert diam == 0.01
    assert round(diam_precise, 4) == 0.0057
