import numpy as np
import pandas as pd
import networkx as nx


class Network:
    def __init__(self):
        self.graph = nx.OrderedDiGraph()

        self.srces = pd.DataFrame(columns=["name", "m_dot_m3/s", "p_kW", "t_deg.C"])
        self.sinks = pd.DataFrame(columns=["name", "op_%", "p_kW", "bypass"])
        self.pipes = pd.DataFrame(columns=["name", "length_m", "diameter_m"])


def create_empty_network():
    """
    Create an empty network

    :return: a Network object that will contain all the network components.
    """
    return Network()
