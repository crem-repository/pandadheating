import math

def design_pipe(fluid, p_nom, delta_temp=15.0, v_max=2.0):
    """
    Estimate mass flow and pipe diamter for a given delta of temperature and maximum flow velocity
    """
    assert delta_temp > 0.0
    m_dot = p_nom / (fluid.Cp * delta_temp)
    diameter = 2*math.sqrt(m_dot/(fluid.rho * v_max))
    # Round to 0.005 m + 0.005
    return m_dot, math.floor(diameter*2E2)/2E2 + 0.005, diameter

